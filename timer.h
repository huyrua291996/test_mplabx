/* 
 * File:   timer.h
 * Author: huyru
 *
 * Created on December 11, 2019, 9:33 AM
 */

#ifndef TIMER_H
#define	TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
typedef enum
{
    STAGE_1,
    STAGE_2,
} test_state_t;
    
    void Timer_Init(void);


#ifdef	__cplusplus
}
#endif

#endif	/* TIMER_H */

