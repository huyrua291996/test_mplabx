/**
  EEPROM SPI Source File

  Company:
    Microchip Technology Inc.

  File Name:
    eeprom_spi.c

  Summary:
    This is the source file containing the EEPROM SPI functions.
	
  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB� Code Configurator - v2.25.2
        Device            :  PIC16F1719
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.34
        MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

#include "eeprom.h"

void SPI_ByteWrite (uint8_t *addressBuffer, uint8_t addlen, uint8_t byteData)
{
    uint8_t check;
    
    //Toggle CS line to start operation
    CS_LAT = 0;
    
    //Send Write Enable command
    SPI_Exchange8bit(EEPROM_WREN);
    
    //Toggle CS line to end operation
    CS_LAT = 1;
    
    //Check if WEL bit is set
    //while(check != 2)                  
      //  check = SPI_ReadStatusRegister();
    __delay_ms(25);    
    //Toggle CS line to start operation
    CS_LAT = 0;
    
    //Send Write Command
    SPI_Exchange8bit(EEPROM_WRITE_EN);
    //Send address byte/s
    SPI_Exchange8bitBuffer(addressBuffer,addlen,NULL);
    //Send data byte
    SPI_Exchange8bit(byteData);
    
    //Toggle CS line to end operation
    CS_LAT = 1;
    
}

uint8_t SPI_ByteRead (uint8_t *addressBuffer, uint8_t addlen)
{
    uint8_t readByte;
    
    //Toggle CS line to start operation
    CS_LAT = 0;
    
    //Send Read Command
    SPI_Exchange8bit(EEPROM_READ_EN);
    //Send address bytes
    SPI_Exchange8bitBuffer(addressBuffer,addlen,NULL);
    //Send Dummy data to clock out data byte from slave
    readByte = SPI_Exchange8bit(DUMMY_DATA);
    
    //Toggle CS line to end operation
    CS_LAT = 1;
    
    //return data byte read
    return(readByte);
}

uint8_t SPI_ReadStatusRegister(void)
{
    uint8_t statusByte;
    
    //Toggle CS line to start operation
    CS_LAT = 0;
    
    //Send Read Status Register Operation
    SPI_Exchange8bit(EEPROM_RDSR);
    //Send Dummy data to clock out data byte from slave
    statusByte = SPI_Exchange8bit(DUMMY_DATA);
    
    //Toggle CS line to end operation
    CS_LAT = 1;
    
    //return data byte read
    return(statusByte);
}

uint8_t SPI_WritePoll(void)
{
    uint8_t pollByte;
    
    //Read the Status Register
    pollByte = SPI_ReadStatusRegister();
    
    //Check if WEL and WIP bits are still set
    while(pollByte == 3)
    {
       pollByte = SPI_ReadStatusRegister(); 
    }
    
    //return 1 if WEL and WIP bits are cleared and the write cycle is finished
    return(1);
}

void SPI_SequentialWrite(uint8_t *addressBuffer, uint8_t addlen, uint8_t *writeBuffer, uint8_t buflen)
{
    //Toggle CS line to begin operation 
    CS_LAT = 0;
    
    //Send Write Enable Command
    SPI_Exchange8bit(EEPROM_WREN);
    
    //Toggle CS line to end operation
    CS_LAT = 1;
    
    //Toggle CS line to start operation
    CS_LAT = 0;
    
    //Send Write Command
    SPI_Exchange8bit(EEPROM_WRITE_EN);
    //Send address bytes
    SPI_Exchange8bitBuffer(addressBuffer,addlen,NULL);
    //Send data bytes to be written
    SPI_Exchange8bitBuffer(writeBuffer,buflen,NULL);
    
    //Toggle CS line to end operation
    CS_LAT = 1;
       
}

uint8_t SPI_SequentialRead(uint8_t *addressBuffer,uint8_t addlen, uint8_t *readBuffer, uint8_t buflen)
{
    //Toggle CS line to begin operation
    CS_LAT = 0;
    
    //Send Read Command
    SPI_Exchange8bit(EEPROM_READ_EN);
    //Send Address bytes
    SPI_Exchange8bitBuffer(addressBuffer,addlen,NULL);
    //Send dummy/NULL data to clock out data bytes from slave
    SPI_Exchange8bitBuffer(NULL,buflen,readBuffer);
    
    //Toggle CS line to end operation
    CS_LAT = 1;
}
      
