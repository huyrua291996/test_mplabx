#include <xc.h>
#include "timer.h"
#include <stdint.h>

extern uint16_t frequencyPress, timePress;
extern test_state_t testState;

void Timer_Init(void)
{
    T0CON0bits.T0EN = 1;
    T0CON0bits.T016BIT = 1; //16bit
    T0CON0bits.T0OUTPS = 0;
    T0CON1bits.T0CS0 = 0;
    T0CON1bits.T0CS1 = 1;
    T0CON1bits.T0CS2 = 0; // Fosc / 4
    T0ASYNC = 1; //not sync
    TMR0H = 0xFC; //60us
    TMR0L = 0x3F;
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    PIR0bits.TMR0IF = 0;
    PIE0bits.TMR0IE = 1;
}

void interrupt myISR(void)
{
    static uint32_t time = 0, time_stage = 0;
    PIR0bits.TMR0IF = 0;
    TMR0H = 0xFC;
    TMR0L = 0x3F;
    time_stage++;
    //time++;

    if (time_stage >= 84000)
    {
        /*if (time >= (1000*frequencyPress))
        {
            LATA &= ~(1<<3);
            time_stage++;
            time = 0;
        }
        if (time == timePress)
        {
            LATA |= (1<<3);
        }*/
        LATD ^= (1<<3);
        
        time_stage = 0;
    }
}



