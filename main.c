/*
 * File:   main.c
 * Author: huyru
 *
 * Created on December 11, 2019, 9:20 AM
 */
#pragma config FEXTOSC = OFF    // External Oscillator mode selection bits (Oscillator not enabled)
#pragma config RSTOSC = HFINTPLL // Power-up default value for COSC bits (HFINTOSC with OSCFRQ= 32 MHz and CDIV = 1:1)
#pragma config CLKOUTEN = OFF   // Clock Out Enable bit (CLKOUT function is disabled; i/o or oscillator function on OSC2)
#pragma config CSWEN = ON       // Clock Switch Enable bit (Writing to NOSC and NDIV is allowed)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (FSCM timer disabled)

// CONFIG2
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR pin is Master Clear function)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config LPBOREN = OFF    // Low-Power BOR enable bit (ULPBOR disabled)
#pragma config BOREN = ON       // Brown-out reset enable bits (Brown-out Reset Enabled, SBOREN bit is ignored)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (VBOR) set to 1.9V on LF, and 2.45V on F Devices)
#pragma config ZCD = OFF        // Zero-cross detect disable (Zero-cross detect circuit is disabled at POR.)
#pragma config PPS1WAY = ON     // Peripheral Pin Select one-way control (The PPSLOCK bit can be cleared and set only once in software)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Stack Overflow or Underflow will cause a reset)

// CONFIG3
#pragma config WDTCPS = WDTCPS_31// WDT Period Select bits (Divider ratio 1:65536; software control of WDTPS)
#pragma config WDTE = OFF       // WDT operating mode (WDT Disabled, SWDTEN is ignored)
#pragma config WDTCWS = WDTCWS_7// WDT Window Select bits (window always open (100%); software control; keyed access not required)
#pragma config WDTCCS = SC      // WDT input clock selector (Software Control)

// CONFIG4
#pragma config BBSIZE = BB512   // Boot Block Size Selection bits (512 words boot block size)
#pragma config BBEN = OFF       // Boot Block Enable bit (Boot Block disabled)
#pragma config SAFEN = OFF      // SAF Enable bit (SAF disabled)
#pragma config WRTAPP = OFF     // Application Block Write Protection bit (Application Block not write protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block not write protected)
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration Register not write protected)
#pragma config WRTSAF = OFF     // Storage Area Flash Write Protection bit (SAF not write protected)
#pragma config LVP = ON         // Low Voltage Programming Enable bit (Low Voltage programming enabled. MCLR/Vpp pin function is MCLR.)

// CONFIG5
#pragma config CP = OFF         // UserNVM Program memory code protection bit (UserNVM code protection disabled)

#include <xc.h>
#include "timer.h"
#include <stdint.h>
#include "EEPROM/eeprom.h"
#include "RF/spi.h"


test_state_t testState = STAGE_1;
uint16_t frequencyPress, timePress;

void main(void) {
    OSCCON2bits.COSC0 = 1;
    OSCCON2bits.COSC1 = 0;
    OSCCON2bits.COSC2 = 0; // 32 pll 2x
    OSCCON2bits.CDIV0 = 0;
    OSCCON2bits.CDIV1 = 0;
    OSCCON2bits.CDIV2 = 0;
    OSCCON2bits.CDIV3 = 0; // 1 : 1
    OSCFRQbits.HFFRQ0 = 0;
    OSCFRQbits.HFFRQ1 = 1;
    OSCFRQbits.HFFRQ2 = 1; //32MHz
    OSCENbits.HFOEN = 1;
    //Timer_Init();
    SPI_Initialize();
    /*TRISDbits.TRISD3 = 0;
    ANSELDbits.ANSD3 = 0;
    ODCONDbits.ODCD3 = 0;
    TRISAbits.TRISA3 = 0;
    ANSELAbits.ANSA3 = 0;
    ODCONAbits.ODCA3 = 0;
    LATDbits.LATD3 = 1;
    LATAbits.LATA3 = 1;*/
    //uint8_t     writeBuffer[] = {0x1A, 0x2A, 0x4A, 0x8A} ;
    //uint8_t     readBuffer[10];
    uint8_t     addressBuffer[] = {0xAB}; // Store the address you want to access here
    uint8_t     readByte;
    
    
    //Writes one byte to the address specified

    while(1)
    {
        /*switch (testState)
        {
            case STAGE_1:
                frequencyPress = 5;
                timePress = 1;
                break;
            case STAGE_2:
                frequencyPress = 10;
                timePress = 2;
                break;
            default: break;
        }*/
        SPI_ByteWrite(&addressBuffer, sizeof (addressBuffer), 0xA5);

        //Wait for write cycle to complete
        //SPI_WritePoll();
        __delay_ms(25);
        //Reads one byte of data from the address specified
        readByte = SPI_ByteRead(&addressBuffer, sizeof (addressBuffer));
        __delay_ms(25);
    }
    return;
}
